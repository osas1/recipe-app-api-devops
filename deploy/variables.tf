variable "prefix" {
  default = "raade"
}

variable "project" {
  default = "recipie-app-api-devops"
}

variable "contact" {
  default = "email@ontarioappdev.com"
}

variable "db_username" {
  description = " Username for the RDS postgress instance"
}

variable "db_password" {
  description = "Password for the RDS postgress instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion" # name should match with instance key in AWS infrastructure
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "695245289132.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "695245289132.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}